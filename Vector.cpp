#include "Vector.h"
#include <stdlib.h>
#include <iostream>
#include <string.h>

using namespace std;

#define FACTOR 5;
#define MAX 10

////////////////////////////////////
//Part A:

//c'tor
Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	
	_size = 0;
	_capacity = n;
	_resizeFactor = n;
	_elements = new int[_resizeFactor];
}

//d'tor
Vector::~Vector()
{
	delete[] _elements;
	_elements = NULL;
}

int Vector::size() const
{
	return _size;
}

int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

bool Vector::empty() const
{
	return _size == 0;
}
////////////////////////////////////
//Part B:

//add val at end of _elements
void Vector::push_back(const int& val)
{
	// check if array is full
	if (_size == _capacity)
	{
		reserve(_resizeFactor + _capacity);
	}

	// add at the end of the array
	_elements[_size] = val;
	_size++;
}

//pop val from the end of _elements
int Vector::pop_back()
{
	if (empty())
	{
		cout << "error: pop from empty vector" << endl;
		return -9999;
	}
	_size--;
	return _elements[_size];
}

//check that _capacity == n
void Vector::reserve(int n)
{
	if (_capacity < n)
	{
		_capacity = n;

		//same the old elements
		int i = 0;
		int* temp = new int[_size];
		for (i = 0; i < _size; i++)
		{
			temp[i] = _elements[i];
		}


		_elements = new int[_capacity];
		for (int i = 0; i < _size; i++)
		{
			_elements[i] = temp[i];
		}
	}
	//delete[] temp;
}

//change _size to n, unless n is greater than the vector's capacity
void Vector::resize(int n)
{
	reserve(n);
	_size = n;	
}

//same as above, if new elements added their value is val
void Vector::resize(int n, const int& val)
{
	int first = _size, i = 0;

	resize(n);

	for (i = first; i <_size; i++)
	{
		_elements[i] = val;
	}
}

//assigns val to all elements
void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < _size; i++)
	{
		_elements[i] = val;
	}
}


////////////////////////////////////
//Part C:

// copy c'tor
Vector::Vector(const Vector& other)
{
	_size = other._size;
	_capacity = other._capacity;
	_resizeFactor = other._resizeFactor;

	//clear _elements
	delete[] _elements;
	_elements = new int[other._capacity];
	//put other._elements in *this
	int i;
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}

//operator =
Vector& Vector::operator=(const Vector& other)
{// same as 'copy c'tor', but return Vector& 
	_size = other._size;
	_capacity = other._capacity;
	_resizeFactor = other._resizeFactor;

	//clear _elements
	delete[] _elements;
	_elements = new int[other._capacity];
	//put other._elements in *this
	int i;
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}

	//return the new Vector
	return *this;
}
////////////////////////////////////
//Part D:
int& Vector::operator[](int n) const
{
	if (n > _size || n < 0)
	{
		cout << "error: Illegal index!\nSystem return the first val" << endl;
		return _elements[0];
	}
	return _elements[n];
}
////////////////////////////////////
void Vector::print()
{
	cout << "vector's  elements:" << endl;
	int i = 0;
	for (i = 0; i < _size; i++)
	{
		cout << _elements[i] << " ";
	}
	cout << endl;
}
////////////////////////////////////