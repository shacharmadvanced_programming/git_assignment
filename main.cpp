#include "Vector.h"

using namespace std;

void partA()
{
	Vector vec(6);
	cout <<"vetor._size: " << vec.size() << endl;
	cout <<"vetor._capacity: " << vec.capacity() << endl;
	cout <<"vetor._resizeFactor: " << vec.resizeFactor() << endl;
	cout <<"Is the vector empty: " << vec.empty() << endl;
}

void partB()
{
	Vector vec(2);
	vec.push_back(4);
	vec.push_back(5);
	vec.push_back(6);// there is a call in push_back to reserve
	vec.push_back(7);// there is a call in push_back to reserve
	cout << "last val in _elements: " << vec.pop_back() << endl;
	cout << "\"new\" last val in _elements: " << vec.pop_back() << endl;
	vec.print();

	vec.resize(10,3);
	vec.print();
	
	vec.assign(888);
	vec.print();
}

void partC()
{
	//make a new Vector
	Vector vec(2);
	vec.push_back(4);
	vec.push_back(5);
	vec.push_back(6);
	vec.push_back(7);
	cout << "first ";
	vec.print();

	// copy c'tor
	Vector vec2(vec);
	cout << "(copy c'tor) ";
	vec2.print();

	//operator =
	Vector vec3(4);
	vec3 = vec;
	cout << "(operator=) ";
	vec3.print();

}

void partD()
{
	//make a new Vector
	Vector vec(2);
	vec.push_back(4);
	vec.push_back(5);
	vec.push_back(6);
	vec.push_back(7);

	cout << "vec[3] = " << vec[3] << endl;
	cout << "vec[30] = " << vec[30] << endl;
}

int main()
{
	cout << "part A:" << endl;
	partA();
	system("pause");
	system("cls");

	cout << "part B:" << endl;
	partB();
	system("pause");
	system("cls");

	cout << "part C:" << endl;
	partC();
	system("pause");
	system("cls");

	cout << "part D:" << endl;
	partD();

	system("pause");
	return 0;
}